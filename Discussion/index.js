// alert("Hello!");

/*
FUNCTIONS
	Function in javascript are lines/block of codes that tell our device/application to perform a certain task when called/invoked

	Functions are mostly created to create complicated tasks to run several lines of code in succession

	Function are also used to prevent repeating lines/block of codes that perform the same task/function
*/

// Function Declaration

/*
	(function statement) defines a function with the specified parameters

	Syntax:
		function functionName() {
			codeblock(statement)
		}

	function keyword - used to defined a javascript functions
	functionName - the function name. Function are named to be able to use later in the code
	function block({}) - the statement which comprise the body of the function. This is where the code to be executed
*/

console.log("My name is John Outside!");

function printName() {
	console.log("My name is John!");
}

// Function Invocation
/*
	The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
*/

printName();

// Function Declaration vs Function Expressions
/*
	Function Declaration
		A function can be created through function declaration by using the function keyword and adding the function name

		Declared function are NOT executed immediately. They are "save for later used" and will be executed later when they are invoked(called).
*/
	// Hoisting
	declaredFunction();

	function declaredFunction() {
		console.log("Hello World from the declaredFunction()");
	}

	declaredFunction();

/*
	Function Expressions
		A function can also be stored in a variable. This is called function expression

		A function expression is an anonymous function assigned to the variableFunction

		Anonymous function - a function without a name
*/

	let variableFunction = function() {
		console.log("Hello from the variableFunction");
	}

// Function expression are ALWAYS invoked(called) usig the variable name.

	variableFunction();

	let funcExpression = function funcName(){
		console.log("Hello from the other side!");

	}

	funcExpression();

// Can we reassign declared functions and function expressions to a NEW anonymous functions? YES!!

	declaredFunction = function(){
		console.log("Updated declaredFunction");
	}

	declaredFunction();

	funcExpression = function () {
		console.log("Updated funcExpression");
	}

	funcExpression();

//We cannot reassign a function expression if its constant

	const constantFunc = function() {
		console.log("Initialized with CONST");
	}

	constantFunc();

	// constantFunc = function() {
	// 	console.log("Can we reassigned?");
	// }

	constantFunc();

// Function Scoping
/*
	Scope is the accessibility (visibility) of variables

	JS Variables has 3 type of scope
		1. local/block scope
		2. global scope
		3. function scope
*/

{
	let localVar = "I am a local variable.";
	console.log(localVar);
}

let globalVar = "I am a global variable.";
console.log(globalVar);

function showNames() {
	// Function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();
/*
	The variables funtionVar, functionConst, and funcionLet are function scoped variables. They can only be accessed inside of the function they were declared in.
*/
	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);

// Nested Function
/*
	You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable "name" as they are within the same code bloc/scope
*/

	function myNewFunction () {
		let name = "Maria";

		function nestedFunction () {
			let nestedName = "Jose";
			console.log(name);
		}

		nestedFunction();
	}

	myNewFunction();
	// nestedFunction(); this will cause an error

// Function and Global Scoped Variables

	// Global Variable
	let globalName = "Erven Joshua";

	function myNewFunction2() {
		let nameInside = "Jenno";

		console.log(globalName);
	}

	myNewFunction2();
	// console.log(nameInside);

// Using alert()
/*
	Syntax:
		alert(message)
*/

	// alert("Hello World!");

	function showSampleAlert () {
		alert("Hello User!");
	}

	// showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed!");

// Using propmt()
/*
	Syntax:
		prompt("<DialogInString>");
*/

	// let samplePropmt = prompt("Enter your name:");
	// console.log("Hello, "+samplePropmt);
	// console.log(typeof samplePropmt);

	function printWelcomeMessage() {
		let firstName = prompt("Enter your First Name: ");
		let lastName = prompt("Enter your Last Name: ");
		console.log("Hello "+ firstName +' '+lastName +"!");
		console.log("Welcome to my page!");

		// alert("Hello "+ firstName +' '+lastName +"!");
		// alert("Welcome to my page!");
	}

	// printWelcomeMessage();

// Function Naming Conventions
/*
	Name your functions in small caps. Follows camelCase when naming variables and functions
*/

	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}

	displayCarInfo();

	// Function names should be definitive of the task it will perform. It usually contains a verb

	function getCourse() {
		let courses = ["Science 101","Math 101","English 101"];
		console.log(courses);
	}

	getCourse();

	// Avoid generic names to avoid confusion within your code

	// Avoid pointless and inappropriate function names
	function foo() {
		console.log(25 % 5);
	}

	foo();