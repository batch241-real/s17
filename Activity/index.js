// console.log("Hello World!"); check if connected
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getInfo () {
		let fullName = prompt("What is your name? ");
		let age = prompt("What is your age? ");
		let location = prompt("What is your location? ");
		alert("Thank you for your input!");
		console.log("Hello, "+ fullName);
		console.log("You are "+ age +"yrs old");
		console.log("You live in "+ location);
	}

	getInfo();

	


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayFavArtist() {
		console.log("1. The Carpenters");
		console.log("2. Paramore");
		console.log("3. Taylo Swift");
		console.log("4. MYMP");
		console.log("5. Parokya ni Edgar");
	}

	displayFavArtist();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayFavMovies() {
		console.log("1. Harry Potter and the Deathly Hallows: Part 1");
		console.log("Rotten Tomatoes Rating: 77%");
		console.log("2. Harry Potter and the Deathly Hallows: Part 2");
		console.log("Rotten Tomatoes Rating: 96%");
		console.log("3. Harry Potter and the prisoner of the Azkaban");
		console.log("Rotten Tomatoes Rating: 90%");
		console.log("4. Harry Potter and the Goblet of Fire");
		console.log("Rotten Tomatoes Rating: 88%");
		console.log("5. Harry Potter and the half-blood Prince");
		console.log("Rotten Tomatoes Rating: 84%");
	}

	displayFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printFriends();
let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printFriends();
// console.log(friend1);
// console.log(friend2);